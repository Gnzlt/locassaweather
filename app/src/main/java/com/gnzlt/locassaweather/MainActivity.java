package com.gnzlt.locassaweather;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.gnzlt.locassaweather.network.interfaces.YahooWeatherApi;
import com.gnzlt.locassaweather.models.ApiItem;
import com.gnzlt.locassaweather.utils.Constants;
import com.gnzlt.locassaweather.utils.NetworkUtils;
import com.gnzlt.locassaweather.utils.Utils;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    private GoogleApiClient mGoogleApiClient;
    private YahooWeatherApi mYahooWeatherApi;

    private BottomSheetBehavior<LinearLayout> bottomSheetBehavior;
    private Call<ApiItem> weatherCall;
    private LatLng lastLocation;
    private GoogleMap map;

    private FrameLayout loadingLayout;
    private FloatingActionButton fab;
    private TextView tv_description;
    private ProgressBar progressBar;
    private TextView tv_title;
    private TextView tv_error;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mYahooWeatherApi = NetworkUtils.getRetrofit().create(YahooWeatherApi.class);
        setupView();

        if (Utils.checkGooglePlayServices(this)) {
            if (Utils.arePermissionsGranted(this)) {
                setupGoogleApi();
            } else {
                Utils.requestPermissions(this);
            }
        }
    }

    protected void onStart() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
        super.onStart();
    }

    protected void onStop() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    private void setupGoogleApi() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(Bundle bundle) {
                            getLastLocation();
                        }

                        @Override
                        public void onConnectionSuspended(int i) {
                        }
                    })
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    private void setupView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                map = googleMap;
                setupMap();
            }
        });

        LinearLayout bottomSheetView = (LinearLayout) findViewById(R.id.bottom_sheet);
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetView);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(View bottomSheet, int newState) {
                if (map != null) {
                    if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                        map.setPadding(0, 0, 0, Math.round(getResources()
                                .getDimension(R.dimen.bottom_sheet_peek_min_height)));
                    } else if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                        map.setPadding(0, 0, 0, Math.round(getResources()
                                .getDimension(R.dimen.bottom_sheet_peek_height)));

                        if (bottomSheetBehavior.getPeekHeight() == 0) {
                            bottomSheetBehavior.setPeekHeight(Math.round(getResources()
                                    .getDimension(R.dimen.bottom_sheet_peek_min_height)));
                        }
                    }
                    if (lastLocation != null) {
                        map.animateCamera(CameraUpdateFactory.newLatLngZoom(lastLocation, 13));
                    }
                }
            }

            @Override
            public void onSlide(View bottomSheet, float slideOffset) {
            }
        });

        fab = (FloatingActionButton) findViewById(R.id.fab);

        tv_description = (TextView) bottomSheetView.findViewById(R.id.description);
        tv_title = (TextView) bottomSheetView.findViewById(R.id.place);

        loadingLayout = (FrameLayout) bottomSheetView.findViewById(R.id.loading);
        progressBar = (ProgressBar) bottomSheetView.findViewById(R.id.progress);
        tv_error = (TextView) bottomSheetView.findViewById(R.id.error);
    }

    private void setupMap() {
        if (map != null) {
            map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    setNewLocation(latLng);
                }
            });

            if (Utils.arePermissionsGranted(this)) {
                map.setMyLocationEnabled(true);
                map.setOnMyLocationButtonClickListener(
                        new GoogleMap.OnMyLocationButtonClickListener() {
                            @Override
                            public boolean onMyLocationButtonClick() {
                                getLastLocation();
                                return true;
                            }
                        });
            }
        }
    }

    private void getLastLocation() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            Location lastLocation = LocationServices.FusedLocationApi.
                    getLastLocation(mGoogleApiClient);
            if (lastLocation != null) {
                setNewLocation(new LatLng(lastLocation.getLatitude(),
                        lastLocation.getLongitude()));
            }
        }
    }

    private void setNewLocation(LatLng location) {
        lastLocation = location;

        if (map != null) {
            map.clear();
            map.addMarker(new MarkerOptions().position(location));
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 13));
        }

        getWeatherForLocation(location);
    }

    private void getWeatherForLocation(LatLng location) {
        showLoadingLayout();

        if (NetworkUtils.isConnected(this)) {
            if (weatherCall != null && weatherCall.isExecuted()) weatherCall.cancel();

            String query = getString(R.string.yahoo_query, location.latitude, location.longitude);
            weatherCall = mYahooWeatherApi.getWeather(query);
            weatherCall.enqueue(new Callback<ApiItem>() {
                @Override
                public void onResponse(Call<ApiItem> call, Response<ApiItem> rawResponse) {
                    if (rawResponse.isSuccessful()) {
                        showWeatherDataFromResponse(rawResponse.body());
                    } else {
                        showError("Error processing the response");
                    }
                }

                @Override
                public void onFailure(Call<ApiItem> call, Throwable t) {
                    showError(t.getMessage());
                }
            });
        } else {
            showError("No internet connection");
        }
    }

    private void showWeatherDataFromResponse(final ApiItem response) {
        if (!response.getTitle().equals("City not found")) {
            tv_title.setText(response.getTitle().split(",")[0]);
            tv_title.setCompoundDrawablePadding(10);
            tv_title.setCompoundDrawablesWithIntrinsicBounds(
                    Utils.getDrawableForWeatherCode(getResources(),
                            response.getCondition().getCode()),
                    null,
                    null,
                    null);
            String description = Html.fromHtml(response.getDescription()
                    .split(Constants.BANNED_TEXT_1)[1]
                    .split(Constants.BANNED_TEXT_2)[0])
                    .toString();
            tv_description.setText(description);
            fab.setVisibility(View.VISIBLE);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(response.getLink()));
                    startActivity(intent);
                }
            });

            setColorFromResponse(response.getCondition().getTemp());

            loadingLayout.setVisibility(View.GONE);
        } else {
            showError("City not found");
        }
    }

    private void setColorFromResponse(int temperature) {
        int color = Utils.getColorFromTemperature(temperature);

        toolbar.setBackgroundColor(color);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Utils.darkerColor(color));
        }
    }

    private void showError(String message) {
        Log.e(TAG, message);
        tv_error.setText(message);
        showErrorLayout();
    }

    private void showLoadingLayout() {
        loadingLayout.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        tv_error.setVisibility(View.GONE);
        fab.setVisibility(View.GONE);

        if (bottomSheetBehavior != null && bottomSheetBehavior.getState()
                != BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        }
    }

    private void showErrorLayout() {
        loadingLayout.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        tv_error.setVisibility(View.VISIBLE);
        fab.setVisibility(View.GONE);

        if (bottomSheetBehavior != null && bottomSheetBehavior.getState()
                != BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] results) {
        if (requestCode == Utils.PERMISSIONS_RESOLUTION_REQUEST) {
            if (results.length == 1
                    && results[0] == PackageManager.PERMISSION_GRANTED) {
                setupMap();
                setupGoogleApi();
            } else {
                Toast.makeText(MainActivity.this, "Locassa Weather needs location permission",
                        Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }
}

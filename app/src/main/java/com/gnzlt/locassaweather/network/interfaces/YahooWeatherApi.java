package com.gnzlt.locassaweather.network.interfaces;

import com.gnzlt.locassaweather.models.ApiItem;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface YahooWeatherApi {

    @GET("v1/public/yql?format=json")
    Call<ApiItem> getWeather(@Query("q") String query);
}

package com.gnzlt.locassaweather.network.deserializers;

import com.gnzlt.locassaweather.models.ApiItem;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class ResponseDeserializer implements JsonDeserializer<ApiItem> {
    @Override
    public ApiItem deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
            throws JsonParseException {
        JsonElement content = json.getAsJsonObject().get("query");
        JsonElement results = content.getAsJsonObject().get("results");
        JsonElement channel = results.getAsJsonObject().get("channel");
        JsonElement item = channel.getAsJsonObject().get("item");

        return new Gson().fromJson(item, ApiItem.class);
    }
}

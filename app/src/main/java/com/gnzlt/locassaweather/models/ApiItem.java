package com.gnzlt.locassaweather.models;

import java.util.ArrayList;

public class ApiItem {
    private String title;
    private String description;
    private String pubDate;
    private String link;
    private ApiCondition condition;
    private ArrayList<ApiForecast> forecast;

    public ApiItem() {
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getPubDate() {
        return pubDate;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public ApiCondition getCondition() {
        return condition;
    }

    public ArrayList<ApiForecast> getForecast() {
        return forecast;
    }
}

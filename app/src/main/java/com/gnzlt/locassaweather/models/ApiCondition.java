package com.gnzlt.locassaweather.models;

public class ApiCondition {
    private int code;
    private String date;
    private int temp;
    private String text;

    public ApiCondition() {
    }

    public int getCode() {
        return code;
    }

    public String getDate() {
        return date;
    }

    public int getTemp() {
        return temp;
    }

    public String getText() {
        return text;
    }
}

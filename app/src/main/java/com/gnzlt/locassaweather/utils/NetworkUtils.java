package com.gnzlt.locassaweather.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.gnzlt.locassaweather.network.deserializers.ResponseDeserializer;
import com.gnzlt.locassaweather.models.ApiItem;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class NetworkUtils {
    public static boolean isConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }

    public static Retrofit getRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(getGsonConverter())
                .build();
    }

    private static Converter.Factory getGsonConverter() {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(ApiItem.class, new ResponseDeserializer()).create();
        return GsonConverterFactory.create(gson);
    }
}

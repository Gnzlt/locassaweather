package com.gnzlt.locassaweather.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.gnzlt.locassaweather.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

public final class Utils {
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 100;
    public static final int PERMISSIONS_RESOLUTION_REQUEST = 101;
    private static final int MIDDLE_TEMPERATURE = 80; // Fahrenheit

    public static boolean checkGooglePlayServices(Context context) {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(context);
        if (result != ConnectionResult.SUCCESS) {
            if (googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog((Activity) context, result,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }
            return false;
        }
        return true;
    }

    public static boolean arePermissionsGranted(Context context) {
        return ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    public static void requestPermissions(Context context) {
        String[] permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};
        ActivityCompat.requestPermissions((Activity) context, permissions,
                PERMISSIONS_RESOLUTION_REQUEST);
    }

    public static int darkerColor(int color) {
        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        hsv[2] *= 0.8f;
        return Color.HSVToColor(hsv);
    }

    public static int getColorFromTemperature(int temperature) {
        int color;
        if (temperature >= MIDDLE_TEMPERATURE) {
            int current = temperature - MIDDLE_TEMPERATURE;
            color = Color.argb(200, Math.round(current * 2.55f), 0, 0);
        } else {
            int current = MIDDLE_TEMPERATURE - temperature;
            color = Color.argb(200, 0, 0, Math.round(current * 2.55f));
        }
        return color;
    }

    public static Drawable getDrawableForWeatherCode(Resources resources, int code) {
        int drawable;
        switch (code) {
            case 16:
                drawable = R.drawable.weather_snowy;
                break;
            case 20:
                drawable = R.drawable.weather_fog;
                break;
            case 26:
                drawable = R.drawable.weather_cloudy;
                break;
            case 44:
                drawable = R.drawable.weather_partlycloudy;
                break;
            case 17:
                drawable = R.drawable.weather_hail;
                break;
            case 4:
                drawable = R.drawable.weather_lightning;
                break;
            case 31:
                drawable = R.drawable.weather_night;
                break;
            case 11:
                drawable = R.drawable.weather_pouring;
                break;
            case 12:
                drawable = R.drawable.weather_rainy;
                break;
            case 32:
                drawable = R.drawable.weather_sunny;
                break;
            case 24:
                drawable = R.drawable.weather_windy;
                break;
            default:
                drawable = R.drawable.weather_sunny;
                break;
        }
        return resources.getDrawable(drawable);
    }
}
